<?php

namespace Drupal\views_dynamic_entity_row\Entity\Render;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\views\Entity\Render\TranslationLanguageRenderer;
use Drupal\views\ViewExecutable;
use Drupal\views_dynamic_entity_row\DynamicEntityRowManager;

/**
 * Renders entity translations in their row language.
 */
class DynamicViewModeTranslationLanguageRenderer extends TranslationLanguageRenderer {

  /**
   * EntityTypeManager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public $view;

  /**
   * {@inheritdoc}
   */
  public $languageManager;

  /**
   * {@inheritdoc}
   */
  public $entityType;

  /**
   * DynamicEntityRowManager Object.
   *
   * @var \Drupal\views_dynamic_entity_row\DynamicEntityRowManager
   */
  protected $viewMode;

  /**
   * {@inheritdoc}
   */
  public function __construct(ViewExecutable $view, LanguageManagerInterface $language_manager, EntityTypeInterface $entity_type, EntityTypeManager $entity_type_manager, DynamicEntityRowManager $view_mode) {
    parent::__construct($view, $language_manager, $entity_type);

    $this->entityTypeManager = $entity_type_manager;
    $this->viewMode = $view_mode;
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(array $result) {

    $view_builder = $this->entityTypeManager->getViewBuilder($this->entityType->id());
    /** @var \Drupal\views\ResultRow $row */
    foreach ($result as $row) {
      $entity = $row->_entity;
      $entity->view = $this->view;
      $langcode = $this->getLangcode($row);
      $view_mode = $this->view->rowPlugin->options['view_mode'];

      if ($dynamic_view_mode = $this->viewMode->getDynamicViewMode($entity)) {

        $view_mode = $dynamic_view_mode;
      }

      $this->build[$entity->id()][$langcode] = $view_builder->view($entity, $view_mode, $this->getLangcode($row));
    }
  }

}
