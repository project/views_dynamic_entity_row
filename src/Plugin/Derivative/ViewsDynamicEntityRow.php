<?php

namespace Drupal\views_dynamic_entity_row\Plugin\Derivative;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\Plugin\Derivative\ViewsEntityRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic views row plugin definitions.
 */
class ViewsDynamicEntityRow extends ViewsEntityRow {

  use StringTranslationTrait;

  /**
   * The Views Dynamic Entity Row settings manager.
   *
   * @var \Drupal\views_dynamic_entity_row\DynamicEntityRowManagerInterface
   */
  protected $dynamicRowManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = parent::create($container, $base_plugin_id);
    $instance->dynamicRowManager = $container->get('views_dynamic_entity_row.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      // Add support for entity types which have a views and VDER integration.
      if (
        ($base_table = $entity_type->getBaseTable()) &&
        $this->viewsData->get($base_table) &&
        $this->entityTypeManager->hasHandler($entity_type_id, 'view_builder') &&
        $this->dynamicRowManager->isSupported($entity_type_id)
      ) {
        $this->derivatives[$entity_type_id] = [
          'id' => 'entity:' . $entity_type_id,
          'provider' => 'views_dynamic_entity_row',
          'title' => $this->t('@label (dynamic)', [
            '@label' => $entity_type->getLabel(),
          ]),
          'help' => $this->t('Display the @label', [
            '@label' => $entity_type->getLabel(),
          ]),
          'base' => [$entity_type->getDataTable() ?: $entity_type->getBaseTable()],
          'entity_type' => $entity_type_id,
          'display_types' => ['normal'],
          'class' => $base_plugin_definition['class'],
        ];
      }
    }

    return $this->derivatives;
  }

}
