<?php

namespace Drupal\views_dynamic_entity_row\Plugin\views\row;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\PluginBase;
use Drupal\views\Plugin\views\row\EntityRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Dynamic entity row plugin.
 *
 * @ViewsRow(
 *   id = "dynamic_entity",
 *   deriver = "Drupal\views_dynamic_entity_row\Plugin\Derivative\ViewsDynamicEntityRow"
 * )
 */
class DynamicEntityRow extends EntityRow {

  /**
   * EntityTypeManager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Object of custom service.
   *
   * @var \Drupal\views_dynamic_entity_row\Entity\Render\DynamicViewModeTranslationLanguageRenderer
   */
  protected $viewsDynamicEntityRowManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->viewsDynamicEntityRowManager = $container->get('views_dynamic_entity_row.manager');

    return $instance;
  }

  /**
   * Returns the current renderer.
   *
   * @return \Drupal\views\Entity\Render\EntityTranslationRendererBase
   *   The configured renderer.
   */
  protected function getEntityTranslationRenderer() {
    if (!isset($this->entityTranslationRenderer)) {
      $view = $this->getView();
      $rendering_language = $view->display_handler->getOption('rendering_language');
      $langcode = NULL;
      $dynamic_renderers = [
        '***LANGUAGE_entity_translation***' => 'DynamicViewModeTranslationLanguageRenderer',
        '***LANGUAGE_entity_default***' => 'DefaultLanguageRenderer',
      ];
      if (isset($dynamic_renderers[$rendering_language])) {
        // Dynamic language set based on result rows or instance defaults.
        $renderer = $dynamic_renderers[$rendering_language];
      }
      else {
        if (strpos($rendering_language, '***LANGUAGE_') !== FALSE) {
          $langcode = PluginBase::queryLanguageSubstitutions()[$rendering_language];
        }
        else {
          // Specific langcode set.
          $langcode = $rendering_language;
        }
        $renderer = 'ConfigurableLanguageRenderer';
      }

      $entity_type = $this->getEntityTypeManager()->getDefinition($this->getEntityTypeId());
      if ($renderer == 'DynamicViewModeTranslationLanguageRenderer') {
        $class = '\Drupal\views_dynamic_entity_row\Entity\Render\\' . $renderer;
        $this->entityTranslationRenderer = new $class($view, $this->getLanguageManager(), $entity_type, $this->entityTypeManager, $this->viewsDynamicEntityRowManager);
      }
      else {
        $class = '\Drupal\views\Entity\Render\\' . $renderer;
        $this->entityTranslationRenderer = new $class($view, $this->getLanguageManager(), $entity_type, $langcode);
      }

    }

    return $this->entityTranslationRenderer;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['view_mode']['#title'] = $this->t('Default view mode');
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    $options = $this->entityTypeManager->getStorage('entity_view_mode')->loadByProperties(['id' => $this->entityTypeId . '.' . $this->options['view_mode']]);

    if (!empty($default_option = reset($options))) {
      return $this->t('Autodetect (default: @default)', [
        '@default' => $default_option->label(),
      ]);
    }
    else {
      return $this->t('Autodetect (default not set)');
    }
  }

}
